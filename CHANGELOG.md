# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.5] - 2019-05-18
### Added
- Metadata classes to manage required upload metadata
  - Policy query classes to validate metadata
- Bundler classes to bundle files and metadata
- Uploader classes to upload a bundle with metadata to Ingest
  - Synchronous and Asynchronous interfaces

### Changed
